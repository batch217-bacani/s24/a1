//console.log("Hello World!");

const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);


const address = {
    houseNumber: 258,
    street: "Washington Ave NW",
    state: "California",
    zipcode: 90011
};

const {houseNumber, street, state, zipcode} = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipcode}`);


const crocs = {
    name: "Lolong",
    species: "saltwater crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} =  crocs;
console.log(`${name} was a ${species}. He weighed at ${weight} with a
measurement of ${measurement}.`);

let arrayNumbers = [1, 2, 3, 4, 5]; 
 arrayNumbers.forEach((numbers) => {
    console.log(numbers)
     
 })

 let sum = arrayNumbers.reduce((previousValue, currentValue) => {
    return previousValue + currentValue
 })

 console.log(sum);

 class Dog {
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Daschund");
console.log(myDog);

